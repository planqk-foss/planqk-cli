import {assert} from 'chai';
import {randomString} from "../../../src/lib/random-string";

describe('random-strings.ts', () => {
  it('should return a random string', () => {
    const stringLength = 10;
    const result = randomString(stringLength);

    assert.isNotNull(result);
    assert.equal(result.length, stringLength);
  });
});