import {assert} from 'chai';
import fs from 'node:fs';
import path from 'node:path';

import {getDescriptionText} from '../../../src/lib/up/get-description';

const workDirectory = 'test/resources/test-project'

describe('get-description.ts', () => {
  it('should read text from designated description file', async () => {
    const descriptionText = await getDescriptionText(workDirectory)
    const descriptionTextCheck = fs.readFileSync(path.join(workDirectory, 'README.md'), 'utf8')

    assert.isTrue(descriptionText === descriptionTextCheck, 'description text successfully read and saved');
  });
});
