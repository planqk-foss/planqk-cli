import AdmZip from 'adm-zip';
import {assert} from 'chai';

import {zipProjectFolder} from '../../../src/lib/up/zip';

describe('zip.ts', () => {
  it('should zip project folder and ignore .planqkignore files', async () => {
    const blob = await zipProjectFolder('test/resources/test-project');

    // Check if the blob is not empty and contains the file Dockerfile
    assert.isNotNull(blob);

    const arrayBuffer = await blob.arrayBuffer();
    const buffer = Buffer.from(arrayBuffer);

    const zip = new AdmZip(buffer);
    const zipEntries = zip.getEntries();

    const dockerfileExists = zipEntries.some(entry => entry.entryName === 'Dockerfile');
    assert.isTrue(dockerfileExists, 'Dockerfile should be present in the zip');

    const testIgnoreExists = zipEntries.some(entry => entry.entryName === 'test-ignore.txt');
    assert.isFalse(testIgnoreExists, 'test-ignore.txt should be ignored in the zip');
  });
});
