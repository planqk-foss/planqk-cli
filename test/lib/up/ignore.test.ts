import {assert} from 'chai';

import {createIgnoreFileChecker} from '../../../src/lib/up/ignore';

describe('ignore.ts', () => {
  it('should return true if file is in .planqkignore', async () => {
    const ignoreFileChecker = await createIgnoreFileChecker('test/resources/test-project');
    const shouldIgnore = ignoreFileChecker('test-ignore.txt')
    assert.equal(shouldIgnore, true);
  });

  it('should return false if file is not in .planqkignore', async () => {
    const ignoreFileChecker = await createIgnoreFileChecker('test/resources/test-project');
    const shouldIgnore = ignoreFileChecker('requirements.txt')
    assert.equal(shouldIgnore, false);
  });
});
