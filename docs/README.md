# Development

## Run your Commands

```shell
./bin/dev <command>
```

## Enable Debug Output

Set the environment variable `DEBUG=*` to print all the debug output to the screen.
Depending on your shell you may need to escape this with `DEBUG=\*`.
On Windows you can’t set environment variables in line, so you’ll need to run set `DEBUG=*` before running the command.

```shell
DEBUG=* ./bin/dev <command>
```

## Use the PLANQK CLI with our Staging Environment

Open `~/.config/planqk/config.json` and add the `basePath` to the `endpoint` setting.
Your `config.json` file may look like this:

```json
{
  "endpoint": {
    "basePath": "https://34.90.225.20.nip.io"
  }
}
```

You can now run `planqk login` or `./bin/dev login` to log-in using a personal access token from a user in the staging environment.

## Use Telepresence to Debug API Operations in the Backend

> This only works in our staging environment.

1. Install [Telepresence](https://www.telepresence.io).
2. Run `telepresence login ...`, `telepresence connect ...`, and `telepresence intercept ...` to create an intercept.
3. Add the `x-telepresence-intercept-id` header to the `config.json` file to the `endpoint` setting. The file may look like this:
   ```json
   {
     "endpoint": {
       "basePath": "https://34.90.225.20.nip.io",
       "defaultHeaders": {
         "x-telepresence-intercept-id": "<your telepresence intercept id>"
       }
     }
   }
   ```
4. Set your breakpoint in the backend code and run the backend with our Telepresence run config.
5. You can now run commands like `planqk services` or `./bin/dev up` to interact with the backend.

## Package the Tarballs

```shell
npx oclif@latest pack tarballs
```

## Update the OpenAPI Client

```shell
npm install -g @openapitools/openapi-generator-cli

cd src
openapi-generator-cli generate -g typescript-fetch -i https://platform.planqk.de/qc-catalog/docs -o client/planqk
openapi-generator-cli generate -g typescript-fetch -i https://platform.planqk.de/user-service/docs -o client/users

# or

openapi-generator-cli generate -g typescript-fetch -i https://34.90.225.20.nip.io/qc-catalog/docs -o client/planqk
openapi-generator-cli generate -g typescript-fetch -i https://34.90.225.20.nip.io/user-service/docs -o client/users
```

## Generate command overview

```bash
cd ..
npm run build
cd docs
npx oclif readme
```

### Usage

<!-- usage -->
```sh-session
$ npm install -g @planqk/planqk-cli
$ planqk COMMAND
running command...
$ planqk (--version)
@planqk/planqk-cli/2.16.1 darwin-arm64 node-v22.11.0
$ planqk --help [COMMAND]
USAGE
  $ planqk COMMAND
...
```
<!-- usagestop -->

### Commands

<!-- commands -->
* [`planqk autocomplete [SHELL]`](#planqk-autocomplete-shell)
* [`planqk compress`](#planqk-compress)
* [`planqk get-context`](#planqk-get-context)
* [`planqk help [COMMAND]`](#planqk-help-command)
* [`planqk init`](#planqk-init)
* [`planqk list-contexts`](#planqk-list-contexts)
* [`planqk login`](#planqk-login)
* [`planqk logout`](#planqk-logout)
* [`planqk run [SERVICEID]`](#planqk-run-serviceid)
* [`planqk serve`](#planqk-serve)
* [`planqk services`](#planqk-services)
* [`planqk set-context [CONTEXTID]`](#planqk-set-context-contextid)
* [`planqk up`](#planqk-up)
* [`planqk version`](#planqk-version)

## `planqk autocomplete [SHELL]`

Display autocomplete installation instructions.

```
USAGE
  $ planqk autocomplete [SHELL] [-r]

ARGUMENTS
  SHELL  (zsh|bash|powershell) Shell type

FLAGS
  -r, --refresh-cache  Refresh cache (ignores displaying instructions)

DESCRIPTION
  Display autocomplete installation instructions.

EXAMPLES
  $ planqk autocomplete

  $ planqk autocomplete bash

  $ planqk autocomplete zsh

  $ planqk autocomplete powershell

  $ planqk autocomplete --refresh-cache
```

_See code: [@oclif/plugin-autocomplete](https://github.com/oclif/plugin-autocomplete/blob/v3.2.22/src/commands/autocomplete/index.ts)_

## `planqk compress`

Compresses the current project and creates a ZIP file

```
USAGE
  $ planqk compress

DESCRIPTION
  Compresses the current project and creates a ZIP file

EXAMPLES
  $ planqk compress
```

_See code: [src/commands/compress/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/compress/index.ts)_

## `planqk get-context`

Get the current context, i.e., the personal or organization account you are currently working with.

```
USAGE
  $ planqk get-context

DESCRIPTION
  Get the current context, i.e., the personal or organization account you are currently working with.

EXAMPLES
  $ planqk get-context
```

_See code: [src/commands/get-context/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/get-context/index.ts)_

## `planqk help [COMMAND]`

Display help for planqk.

```
USAGE
  $ planqk help [COMMAND...] [-n]

ARGUMENTS
  COMMAND...  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for planqk.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v6.2.25/src/commands/help.ts)_

## `planqk init`

Initialize a PLANQK project to create a service.

```
USAGE
  $ planqk init [--name <value>] [--non-interactive]

FLAGS
  --name=<value>     The name of the service
  --non-interactive  Run it in non-interactive mode

DESCRIPTION
  Initialize a PLANQK project to create a service.

EXAMPLES
  $ planqk init
```

_See code: [src/commands/init/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/init/index.ts)_

## `planqk list-contexts`

Retrieves the available contexts, i.e., the personal or organizational accounts available to you to work with.

```
USAGE
  $ planqk list-contexts

DESCRIPTION
  Retrieves the available contexts, i.e., the personal or organizational accounts available to you to work with.

EXAMPLES
  $ planqk list-contexts
```

_See code: [src/commands/list-contexts/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/list-contexts/index.ts)_

## `planqk login`

Login with your PLANQK credentials

```
USAGE
  $ planqk login [-t <value>]

FLAGS
  -t, --token=<value>  Your personal access token

DESCRIPTION
  Login with your PLANQK credentials

EXAMPLES
  $ planqk login -t <personal access token>
```

_See code: [src/commands/login/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/login/index.ts)_

## `planqk logout`

Logout from PLANQK

```
USAGE
  $ planqk logout

DESCRIPTION
  Logout from PLANQK

EXAMPLES
  $ planqk logout
```

_See code: [src/commands/logout/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/logout/index.ts)_

## `planqk run [SERVICEID]`

Creates a job execution of a PLANQK Service

```
USAGE
  $ planqk run [SERVICEID] [-d <value>] [--data-file <value>] [--detached] [-p <value>] [--params-file
    <value>]

FLAGS
  -d, --data=<value>         Input data as JSON string.
  -p, --params=<value>       Parameters as JSON string.
      --data-file=<value>    Relative path to file containing input data.
      --detached             Executes the job in detached mode, i.e., without waiting for it to finish.
      --params-file=<value>  Relative path to file containing params.

DESCRIPTION
  Creates a job execution of a PLANQK Service

EXAMPLES
  $ planqk run

  $ planqk run --detached

  $ planqk run -d '{"values": [10,12]}' -p '{"round_up": true}'

  $ planqk run --data-file=./input/data.json --params-file=./input/params.json
```

_See code: [src/commands/run/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/run/index.ts)_

## `planqk serve`

Runs your project in a containerized environment and expose it through a local web server, similarly to how PLANQK would run your code. The local web server exposes the same HTTP endpoints to start a service execution, to check the status of running executions, to cancel executions, and to retrieve execution results.

```
USAGE
  $ planqk serve [-p <value>]

FLAGS
  -p, --port=<value>  The port on which the local web server accepts requests

DESCRIPTION
  Runs your project in a containerized environment and expose it through a local web server, similarly to how PLANQK
  would run your code. The local web server exposes the same HTTP endpoints to start a service execution, to check the
  status of running executions, to cancel executions, and to retrieve execution results.

EXAMPLES
  $ planqk serve

  $ planqk serve -p <port>
```

_See code: [src/commands/serve/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/serve/index.ts)_

## `planqk services`

List all services of the current context.

```
USAGE
  $ planqk services

DESCRIPTION
  List all services of the current context.

EXAMPLES
  $ planqk services
```

_See code: [src/commands/services/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/services/index.ts)_

## `planqk set-context [CONTEXTID]`

Set the current context, i.e., the personal or organization account you are currently working with.

```
USAGE
  $ planqk set-context [CONTEXTID]

DESCRIPTION
  Set the current context, i.e., the personal or organization account you are currently working with.

EXAMPLES
  $ planqk set-context

  $ planqk set-context <context-id>
```

_See code: [src/commands/set-context/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/set-context/index.ts)_

## `planqk up`

Creates or updates a PLANQK service

```
USAGE
  $ planqk up [--silent]

FLAGS
  --silent  Suppresses all outputs, helpful when executed in a CI/CD pipeline.

DESCRIPTION
  Creates or updates a PLANQK service

EXAMPLES
  $ planqk up
```

_See code: [src/commands/up/index.ts](https://gitlab.com/planqk-foss/planqk-cli/blob/v2.16.1/src/commands/up/index.ts)_

## `planqk version`

```
USAGE
  $ planqk version [--json] [--verbose]

FLAGS
  --verbose  Show additional information about the CLI.

GLOBAL FLAGS
  --json  Format output as json.

FLAG DESCRIPTIONS
  --verbose  Show additional information about the CLI.

    Additionally shows the architecture, node version, operating system, and versions of plugins that the CLI is using.
```

_See code: [@oclif/plugin-version](https://github.com/oclif/plugin-version/blob/v2.2.25/src/commands/version.ts)_
<!-- commandsstop -->
