#!/usr/bin/env node

// disable warnings
process.removeAllListeners('warning');

// disable SSL/TLS cert verification
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

(async () => {
  const oclif = await import('@oclif/core')
  await oclif.execute({ dir: __dirname })
})()
