#!/usr/bin/env node

// disable warnings
process.removeAllListeners('warning');

// disable SSL/TLS cert verification
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

// always perform version update check
process.env.PLANQK_FORCE_VERSION_CACHE_UPDATE = 'true';

(async () => {
  const oclif = await import('@oclif/core')
  await oclif.execute({ development: true, dir: __dirname })
})()
