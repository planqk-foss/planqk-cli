# planqk-cli

The PLANQK Command Line Interface (CLI) lets you interact with the PLANQK Platform directly from your terminal.
We have installation instructions to guide you through the initial setup in our
[quickstart](https://docs.platform.planqk.de/quickstart.html) guide.

Detailed information about the supported commands can be found in our
[CLI reference](https://docs.platform.planqk.de/cli-reference.html).

## Installation

To install the PLANQK CLI, you must install Node.js and the npm command line interface using either a
[Node version manager](https://github.com/nvm-sh/nvm) or a [Node installer](https://nodejs.org/en/download).

**Make sure you install Node.js version 18 or higher.**

Then install the PLANQK CLI globally using npm:

```bash
npm install -g @planqk/planqk-cli
```

> **IMPORTANT:** Make sure you have **uninstalled** the deprecated CLI:
> ```bash
> npm uninstall -g @anaqor/planqk
> ```

You can use the `--help` flag to get information about the supported commands:

``` bash
planqk --help
```

You may also get information about a specific command:

``` bash
planqk <command> --help
```

> **Troubleshooting:**
> If you experience EACCES permissions errors during installation, we recommend using [Node version manager](https://github.com/nvm-sh/nvm) (nvm).
> Further, you may refer to the official [npm documentation](https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally) how to resolve such issues.

## License

Apache-2.0 | Copyright 2023-2024 Kipu Quantum GmbH
