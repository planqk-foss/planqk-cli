/* tslint:disable */
/* eslint-disable */
/**
 * PlanQK Platform API
 * Part of the OpenAPI specification for the PlanQK Platform.
 *
 * The version of the OpenAPI document: v1
 * Contact: info@anaqor.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ServiceDefinitionCommand
 */
export interface ServiceDefinitionCommand {
    /**
     * 
     * @type {string}
     * @memberof ServiceDefinitionCommand
     */
    type?: ServiceDefinitionCommandTypeEnum;
}


/**
 * @export
 */
export const ServiceDefinitionCommandTypeEnum = {
    Unpublish: 'UNPUBLISH',
    UnpublishForce: 'UNPUBLISH_FORCE'
} as const;
export type ServiceDefinitionCommandTypeEnum = typeof ServiceDefinitionCommandTypeEnum[keyof typeof ServiceDefinitionCommandTypeEnum];


/**
 * Check if a given object implements the ServiceDefinitionCommand interface.
 */
export function instanceOfServiceDefinitionCommand(value: object): value is ServiceDefinitionCommand {
    return true;
}

export function ServiceDefinitionCommandFromJSON(json: any): ServiceDefinitionCommand {
    return ServiceDefinitionCommandFromJSONTyped(json, false);
}

export function ServiceDefinitionCommandFromJSONTyped(json: any, ignoreDiscriminator: boolean): ServiceDefinitionCommand {
    if (json == null) {
        return json;
    }
    return {
        
        'type': json['type'] == null ? undefined : json['type'],
    };
}

export function ServiceDefinitionCommandToJSON(value?: ServiceDefinitionCommand | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'type': value['type'],
    };
}

