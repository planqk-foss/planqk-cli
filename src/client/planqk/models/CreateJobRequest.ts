/* tslint:disable */
/* eslint-disable */
/**
 * PlanQK Platform API
 * Part of the OpenAPI specification for the PlanQK Platform.
 *
 * The version of the OpenAPI document: v1
 * Contact: info@anaqor.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { DataPoolRef } from './DataPoolRef';
import {
    DataPoolRefFromJSON,
    DataPoolRefFromJSONTyped,
    DataPoolRefToJSON,
} from './DataPoolRef';

/**
 * 
 * @export
 * @interface CreateJobRequest
 */
export interface CreateJobRequest {
    /**
     * 
     * @type {string}
     * @memberof CreateJobRequest
     */
    inputData?: string;
    /**
     * 
     * @type {DataPoolRef}
     * @memberof CreateJobRequest
     */
    inputDataRef?: DataPoolRef;
    /**
     * 
     * @type {string}
     * @memberof CreateJobRequest
     */
    parameters?: string;
    /**
     * 
     * @type {DataPoolRef}
     * @memberof CreateJobRequest
     */
    parametersRef?: DataPoolRef;
    /**
     * 
     * @type {boolean}
     * @memberof CreateJobRequest
     */
    persistResult?: boolean;
    /**
     * 
     * @type {string}
     * @memberof CreateJobRequest
     */
    serviceDefinitionId: string;
}

/**
 * Check if a given object implements the CreateJobRequest interface.
 */
export function instanceOfCreateJobRequest(value: object): value is CreateJobRequest {
    if (!('serviceDefinitionId' in value) || value['serviceDefinitionId'] === undefined) return false;
    return true;
}

export function CreateJobRequestFromJSON(json: any): CreateJobRequest {
    return CreateJobRequestFromJSONTyped(json, false);
}

export function CreateJobRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): CreateJobRequest {
    if (json == null) {
        return json;
    }
    return {
        
        'inputData': json['inputData'] == null ? undefined : json['inputData'],
        'inputDataRef': json['inputDataRef'] == null ? undefined : DataPoolRefFromJSON(json['inputDataRef']),
        'parameters': json['parameters'] == null ? undefined : json['parameters'],
        'parametersRef': json['parametersRef'] == null ? undefined : DataPoolRefFromJSON(json['parametersRef']),
        'persistResult': json['persistResult'] == null ? undefined : json['persistResult'],
        'serviceDefinitionId': json['serviceDefinitionId'],
    };
}

export function CreateJobRequestToJSON(value?: CreateJobRequest | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'inputData': value['inputData'],
        'inputDataRef': DataPoolRefToJSON(value['inputDataRef']),
        'parameters': value['parameters'],
        'parametersRef': DataPoolRefToJSON(value['parametersRef']),
        'persistResult': value['persistResult'],
        'serviceDefinitionId': value['serviceDefinitionId'],
    };
}

