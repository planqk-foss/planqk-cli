/* tslint:disable */
/* eslint-disable */
/**
 * PlanQK Platform API
 * Part of the OpenAPI specification for the PlanQK Platform.
 *
 * The version of the OpenAPI document: v1
 * Contact: info@anaqor.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import { CpuConfigurationFromJSONTyped } from './CpuConfiguration';
import { GpuConfigurationFromJSONTyped } from './GpuConfiguration';
import { MemoryConfigurationFromJSONTyped } from './MemoryConfiguration';
/**
 * 
 * @export
 * @interface ResourceConfiguration
 */
export interface ResourceConfiguration {
    /**
     * 
     * @type {string}
     * @memberof ResourceConfiguration
     */
    type?: string;
}

/**
 * Check if a given object implements the ResourceConfiguration interface.
 */
export function instanceOfResourceConfiguration(value: object): value is ResourceConfiguration {
    return true;
}

export function ResourceConfigurationFromJSON(json: any): ResourceConfiguration {
    return ResourceConfigurationFromJSONTyped(json, false);
}

export function ResourceConfigurationFromJSONTyped(json: any, ignoreDiscriminator: boolean): ResourceConfiguration {
    if (json == null) {
        return json;
    }
    if (!ignoreDiscriminator) {
        if (json['type'] === 'CpuConfiguration') {
            return CpuConfigurationFromJSONTyped(json, true);
        }
        if (json['type'] === 'GpuConfiguration') {
            return GpuConfigurationFromJSONTyped(json, true);
        }
        if (json['type'] === 'MemoryConfiguration') {
            return MemoryConfigurationFromJSONTyped(json, true);
        }
    }
    return {
        
        'type': json['type'] == null ? undefined : json['type'],
    };
}

export function ResourceConfigurationToJSON(value?: ResourceConfiguration | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'type': value['type'],
    };
}

