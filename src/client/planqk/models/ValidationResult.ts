/* tslint:disable */
/* eslint-disable */
/**
 * PlanQK Platform API
 * Part of the OpenAPI specification for the PlanQK Platform.
 *
 * The version of the OpenAPI document: v1
 * Contact: info@anaqor.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ValidationResult
 */
export interface ValidationResult {
    /**
     * 
     * @type {string}
     * @memberof ValidationResult
     */
    state?: ValidationResultStateEnum;
    /**
     * 
     * @type {string}
     * @memberof ValidationResult
     */
    summary?: string;
    /**
     * 
     * @type {Array<string>}
     * @memberof ValidationResult
     */
    messages?: Array<string>;
}


/**
 * @export
 */
export const ValidationResultStateEnum = {
    Success: 'SUCCESS',
    Warning: 'WARNING',
    Error: 'ERROR'
} as const;
export type ValidationResultStateEnum = typeof ValidationResultStateEnum[keyof typeof ValidationResultStateEnum];


/**
 * Check if a given object implements the ValidationResult interface.
 */
export function instanceOfValidationResult(value: object): value is ValidationResult {
    return true;
}

export function ValidationResultFromJSON(json: any): ValidationResult {
    return ValidationResultFromJSONTyped(json, false);
}

export function ValidationResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ValidationResult {
    if (json == null) {
        return json;
    }
    return {
        
        'state': json['state'] == null ? undefined : json['state'],
        'summary': json['summary'] == null ? undefined : json['summary'],
        'messages': json['messages'] == null ? undefined : json['messages'],
    };
}

export function ValidationResultToJSON(value?: ValidationResult | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'state': value['state'],
        'summary': value['summary'],
        'messages': value['messages'],
    };
}

