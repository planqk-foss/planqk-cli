/* tslint:disable */
/* eslint-disable */
/**
 * PlanQK Platform API
 * Part of the OpenAPI specification for the PlanQK Platform.
 *
 * The version of the OpenAPI document: v1
 * Contact: info@anaqor.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface AmountMajorDto
 */
export interface AmountMajorDto {
    /**
     * 
     * @type {number}
     * @memberof AmountMajorDto
     */
    value?: number;
    /**
     * 
     * @type {string}
     * @memberof AmountMajorDto
     */
    currency?: string;
}

/**
 * Check if a given object implements the AmountMajorDto interface.
 */
export function instanceOfAmountMajorDto(value: object): value is AmountMajorDto {
    return true;
}

export function AmountMajorDtoFromJSON(json: any): AmountMajorDto {
    return AmountMajorDtoFromJSONTyped(json, false);
}

export function AmountMajorDtoFromJSONTyped(json: any, ignoreDiscriminator: boolean): AmountMajorDto {
    if (json == null) {
        return json;
    }
    return {
        
        'value': json['value'] == null ? undefined : json['value'],
        'currency': json['currency'] == null ? undefined : json['currency'],
    };
}

export function AmountMajorDtoToJSON(value?: AmountMajorDto | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'value': value['value'],
        'currency': value['currency'],
    };
}

