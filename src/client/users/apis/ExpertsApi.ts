/* tslint:disable */
/* eslint-disable */
/**
 * PlanQK User Service API
 * Part of the OpenAPI specification for the PlanQK Platform.
 *
 * The version of the OpenAPI document: 1.0
 * Contact: support@planqk.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  PageUserDto,
  Pageable,
  UserDto,
} from '../models/index';
import {
    PageUserDtoFromJSON,
    PageUserDtoToJSON,
    PageableFromJSON,
    PageableToJSON,
    UserDtoFromJSON,
    UserDtoToJSON,
} from '../models/index';

export interface FindExpertByIdRequest {
    id: string;
}

export interface GetExpertsRequest {
    pageable: Pageable;
}

/**
 * 
 */
export class ExpertsApi extends runtime.BaseAPI {

    /**
     * Returns the expert details with the given id.
     * Returns the expert details with the given id.
     */
    async findExpertByIdRaw(requestParameters: FindExpertByIdRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<UserDto>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling findExpertById().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/experts/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => UserDtoFromJSON(jsonValue));
    }

    /**
     * Returns the expert details with the given id.
     * Returns the expert details with the given id.
     */
    async findExpertById(requestParameters: FindExpertByIdRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<UserDto> {
        const response = await this.findExpertByIdRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Lists all experts.
     * Lists all experts.
     */
    async getExpertsRaw(requestParameters: GetExpertsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PageUserDto>> {
        if (requestParameters['pageable'] == null) {
            throw new runtime.RequiredError(
                'pageable',
                'Required parameter "pageable" was null or undefined when calling getExperts().'
            );
        }

        const queryParameters: any = {};

        if (requestParameters['pageable'] != null) {
            queryParameters['pageable'] = requestParameters['pageable'];
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/experts`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PageUserDtoFromJSON(jsonValue));
    }

    /**
     * Lists all experts.
     * Lists all experts.
     */
    async getExperts(requestParameters: GetExpertsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PageUserDto> {
        const response = await this.getExpertsRaw(requestParameters, initOverrides);
        return await response.value();
    }

}
