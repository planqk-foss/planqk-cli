import {FetchError, ResponseError} from '../client/planqk';
import {debugEnabled} from './debug';
import {getErrorMessage} from './fetch';


export const handleError = async (error: unknown): Promise<string> => {
  if (error instanceof ResponseError) {
    return getErrorMessage(error.response)
  }

  if (error instanceof FetchError) {
    const errorMessage = error.cause ? error.cause.message : error.message
    return `Internal error occurred, please try again later (${errorMessage})`
  }

  if (debugEnabled()) {
    console.error(JSON.stringify(error))
  }

  return 'Internal error occurred, please contact your PLANQK administrator'
}
