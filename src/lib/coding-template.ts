import AdmZip from 'adm-zip'
import YAML from 'yaml'

import {debugEnabled} from './debug'
import {fetchOrThrow, PlanqkError} from './fetch'

export const downloadArchive = async (): Promise<AdmZip> => {
  const config = {
    cache: 'no-cache' as RequestCache,
    method: 'GET',
    mode: 'same-origin' as RequestMode,
    referrerPolicy: 'no-referrer' as ReferrerPolicy,
    responseType: 'arraybuffer' as ResponseType,
  }

  let zip: AdmZip

  try {
    const response = await fetchOrThrow('https://gitlab.com/planqk-foss/planqk-samples/-/archive/main/planqk-samples-main.zip', config)
    const buffer = await response.arrayBuffer()
    zip = new AdmZip(Buffer.from(buffer), {})
  } catch (error) {
    if (error instanceof PlanqkError) {
      const errorMessage = await error.getErrorMessage()
      throw new Error(errorMessage)
    }

    if (debugEnabled()) {
      console.error(JSON.stringify(error))
    }

    throw new Error('Internal error occurred, please contact your PLANQK administrator')
  }

  return zip
}

export const extractTemplate = (zip: AdmZip, templatePath: string, projectLocation: string): void => {
  const templateFolder = `planqk-samples-main/coding-templates/${templatePath}/`
  for (const entry of zip.getEntries()) {
    if (!entry.isDirectory && entry.entryName.startsWith(templateFolder)) {
      let destinationPath = projectLocation

      // check if file is in sub-folder
      if (entry.entryName.replace(templateFolder, '').includes('/')) {
        const pathWithinFolder = entry.entryName.replace(templateFolder, '')
        destinationPath = destinationPath + '/' + pathWithinFolder.slice(0, Math.max(0, pathWithinFolder.lastIndexOf('/')))
      }

      zip.extractEntryTo(entry.entryName, destinationPath, false, true)
    }
  }
}

export const getReadmeTemplate = (zip: AdmZip, templatePath: string): string => {
  const basePath = 'planqk-samples-main/coding-templates/'

  // first part of templatePath defines the template path
  const readmeLocation = templatePath.split('/').shift()

  for (const entry of zip.getEntries()) {
    if (!entry.isDirectory && entry.entryName.startsWith(`${basePath}${readmeLocation}/template-README.md`)) {
      return zip.readAsText(entry)
    }
  }

  throw new Error(`Internal error occurred, please contact your PLANQK administrator: template-README.md not found at '${basePath}${readmeLocation}'`)
}

export interface NameValuePair {
  name: string
  value: string
}

export const getTemplateVariables = (zip: AdmZip, templatePath: string): NameValuePair[] | undefined => {
  const variableFilesLocation = 'planqk-samples-main/coding-templates/.vars/'

  // last part of templatePath is the name of the template
  const templateName = templatePath.split('/').pop()

  for (const entry of zip.getEntries()) {
    if (!entry.isDirectory && entry.entryName.startsWith(variableFilesLocation)) {
      const variableFile = entry.entryName.replace(variableFilesLocation, '')
      if (variableFile.includes(`${templateName}.yaml`)) {
        const data = zip.readAsText(entry)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const yamlObject: any = YAML.parse(data)
        if (yamlObject.vars) {
          return yamlObject.vars as NameValuePair[]
        }
      }
    }
  }

  return undefined
}
