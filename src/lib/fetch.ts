export class PlanqkError extends Error {
  response: Response

  constructor(response: Response) {
    super(response.statusText)
    this.response = response
  }

  async getErrorMessage(): Promise<string> {
    return getErrorMessage(this.response)
  }
}

export const fetchOrThrow = async (input: RequestInfo | URL, init?: RequestInit): Promise<Response> => {
  const response = await fetch(input, init)
  if (!response.ok) {
    throw new PlanqkError(response)
  }

  return response
}

export const getErrorMessage = async (response: Response): Promise<string> => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let body: any
  try {
    body = await response.json()
  } catch {
    // ignore
  }

  const httpStatus = response.status && response.statusText ? `${response.status} - ${response.statusText}` : response.status

  // message is either in body.errorMessage, body.error or body.detail
  let errorMessage
  if (body && body.errorMessage) errorMessage = body.errorMessage
  if (!errorMessage && body && body.error) errorMessage = body.error
  if (!errorMessage && body && body.detail) errorMessage = body.detail

  return errorMessage ? `${errorMessage} (${httpStatus})` : `${httpStatus}`
}
