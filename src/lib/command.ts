import {exec, execSync, StdioOptions} from "node:child_process";

export const executeAsyncCommand = async (command: string): Promise<{ stderr: string, stdout: string }> => {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error)
      } else {
        resolve({stderr, stdout})
      }
    })
  })
}


export const executeCommand = (command: string, stdioOption?: StdioOptions): void => {
  execSync(`${command}`, {stdio: stdioOption ?? 'inherit'})
}
