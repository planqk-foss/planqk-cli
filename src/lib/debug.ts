export const debugEnabled = (): boolean => Boolean(process.env.DEBUG && process.env.DEBUG !== '')
