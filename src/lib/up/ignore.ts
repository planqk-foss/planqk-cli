import ignore from 'ignore';
import fs from 'node:fs';
import path from 'node:path';
import {promisify} from 'node:util';

const readFile = promisify(fs.readFile)

export async function createIgnoreFileChecker(filePath: string = process.cwd()): Promise<(fullPath: string) => boolean> {
  const ignoreFilePath = path.join(filePath, '.planqkignore')

  if (!fs.existsSync(ignoreFilePath)) {
    return () => false
  }

  const ignoreFileContents = await readFile(ignoreFilePath, 'utf8')
  const ig = ignore().add(ignoreFileContents.split('\n'))

  ig.add('planqk.zip')

  return (fullPath: string) => ig.ignores(fullPath)
}
