import fs from 'node:fs';
import path from 'node:path';

function getDescriptionPath(workingDirectory = process.cwd()): string {
  const configPath = path.join(workingDirectory, 'planqk.json')

  if (fs.existsSync(configPath)) {
    const config = JSON.parse(fs.readFileSync(configPath, 'utf8'))
    if (config.descriptionFile) {
      return path.join(workingDirectory, config.descriptionFile)
    }

    return ''
  }

  throw new Error('planqk.json file not found')
}

export async function getDescriptionText(workingDirectory: string = process.cwd()): Promise<string> {
  const descriptionPath = getDescriptionPath(workingDirectory)

  if (descriptionPath) {
    if (fs.existsSync(descriptionPath)) {
      return fs.readFileSync(descriptionPath, 'utf8')
    }

    throw new Error('Provided "descriptionFile" was not found')
  }

  return ''
}
