import AdmZip from 'adm-zip';
import fs from 'fs-extra';
import {tmpdir} from 'node:os';
import path from 'node:path';

import {streamToBlob} from '../stream-to-blob';
import {createIgnoreFileChecker} from './ignore';

export async function zipProjectFolder(workingDirectory: string = process.cwd(),
                                       targetDirectory: string = tmpdir(),
                                       targetFilename = 'user_code.zip'): Promise<Blob> {
  const targetFilePath = path.join(targetDirectory, '/', targetFilename)

  if (fs.existsSync(targetFilePath)) {
    fs.unlinkSync(targetFilePath)
  }

  const zip = new AdmZip()
  const ignoreFile = await createIgnoreFileChecker(workingDirectory)

  function addToZip(folderPath: string) {
    const files = fs.readdirSync(folderPath)

    for (const file of files) {
      const fullPath = path.join(folderPath, file)

      if (ignoreFile(fullPath)) {
        continue
      }

      const stat = fs.statSync(fullPath)
      if (stat.isDirectory()) {
        addToZip(fullPath)
      } else {
        zip.addLocalFile(fullPath, path.relative(workingDirectory, folderPath))
      }
    }
  }

  addToZip(workingDirectory)
  zip.writeZip(targetFilePath)

  const userCode = fs.createReadStream(path.join(targetFilePath))
  return streamToBlob(userCode)
}
