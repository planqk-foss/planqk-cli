import {AuthenticatedCommand} from '../../model/command'

export default class GetContext extends AuthenticatedCommand {
  static description = 'Get the current context, i.e., the personal or organization account you are currently working with.'

  static examples = [
    '$ planqk get-context',
  ]

  async run(): Promise<void> {
    // printing the current context is done by parent 'init' method
  }
}
