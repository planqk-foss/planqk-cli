import {password} from '@inquirer/prompts';
import {Flags, ux} from '@oclif/core'

import {AbstractCommand} from '../../model/command'
import {AuthType} from '../../model/user-config'
import AuthService from '../../service/auth-service'
import {writeUserConfig} from '../../service/user-config-service'

export default class Login extends AbstractCommand {
  static description = 'Login with your PLANQK credentials'

  static examples = [
    '$ planqk login -t <personal access token>',
  ]

  static flags = {
    token: Flags.string({char: 't', description: 'Your personal access token', required: false}),
  }

  async run(): Promise<void> {
    const {flags} = await this.parse(Login)

    let {token} = flags
    if (!token) {
      token = await password({message: 'Enter or paste your personal access token?', mask: true});
    }

    if (!token) {
      throw new Error('Invalid credentials. Please try again.')
    }

    ux.action.start('Login')

    const user = await new AuthService(this.userConfig).authenticate(token)

    const userConfig = {
      ...this.userConfig,
      auth: {
        type: AuthType.API_KEY,
        value: token,
      },
      context: {
        id: user.id!,
        displayName: `${user.firstname!} ${user.lastname!}`,
        isOrganization: false,
      },
    }

    writeUserConfig(this.config.configDir, userConfig)

    ux.action.stop('successful')
  }
}
