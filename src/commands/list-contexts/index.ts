import {ux} from '@oclif/core'
import Table from 'tty-table'

import Account from '../../model/account'
import {AuthenticatedCommand} from '../../model/command'
import PlanqkService from '../../service/planqk-service'

interface Context extends Record<string, unknown> {
  displayName: string
  id: string
  isOrganization: boolean
}

export default class ListContexts extends AuthenticatedCommand {
  static description = 'Retrieves the available contexts, i.e., the personal or organizational accounts available to you to work with.'

  static examples = [
    '$ planqk list-contexts',
  ]

  planqkService!: PlanqkService

  async init(): Promise<void> {
    await super.init()
    this.planqkService = new PlanqkService(this, this.config, this.userConfig)
  }

  async run(): Promise<void> {
    ux.action.start('Loading available contexts')
    const accounts: Account[] = await this.planqkService.getAccounts()
    ux.action.stop('Done')

    const tableData: Context[] = accounts
      .map(account => ({displayName: account.displayName, id: account.id, isOrganization: account.isOrganization}))

    const header = [
      {value: "displayName", alias: "Name", align: "left"},
      {value: "id", alias: "ID", align: "left"},
      {value: "isOrganization", alias: "Account", width: 14, formatter: (value: boolean) => value ? "Organization" : "Personal"},
    ]

    const t = Table(header, tableData)

    ux.stdout(t.render())
    ux.stdout()
  }
}
