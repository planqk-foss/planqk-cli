import {ux} from '@oclif/core'
import Table from "tty-table";

import {AuthenticatedCommand} from '../../model/command'
import PlanqkService from '../../service/planqk-service'

interface Service extends Record<string, unknown> {
  id?: string
  name?: string
}

export default class Services extends AuthenticatedCommand {
  static description = 'List all services of the current context.'

  static examples = [
    '$ planqk services',
  ]

  planqkService!: PlanqkService

  async init(): Promise<void> {
    await super.init()
    this.planqkService = new PlanqkService(this, this.config, this.userConfig)
  }

  async run(): Promise<void> {
    ux.action.start('Loading services')
    const services = await this.planqkService.getServices()
    ux.action.stop('Done')

    const tableData: Service[] = services
      .map(service => ({id: service.id, name: service.name}))

    const header = [
      {value: "id", alias: "ID", align: "left"},
      {value: "name", alias: "Name", align: "left"},
    ]

    const t = Table(header, tableData)

    ux.stdout(`Available services for ${this.userConfig.context?.displayName}:`)
    ux.stdout(t.render())
    ux.stdout()
  }
}
