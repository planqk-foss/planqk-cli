import {confirm} from "@inquirer/prompts";
import {Flags, ux} from '@oclif/core'
import fs from "fs-extra"
import Listr from "listr"
import path from "node:path";
import {executeAsyncCommand} from "../../lib/command"
import {randomString} from "../../lib/random-string";
import {AbstractCommand} from '../../model/command'

export default class Openapi extends AbstractCommand {
  static IMAGE = 'registry.gitlab.com/planqk-foss/planqk-commons:latest'

  static description = 'Generates the OpenAPI description for your project based on the parameter and return types of your run() method. ' +
    'You can use this command to verify how the API of your project would look like when deployed on PLANQK. ' +
    'The output of this command will be used when creating or updating your service.';

  static examples = [
    '$ planqk openapi',
    '$ planqk openapi --file=./openapi.yaml --format=yaml --force',
  ]

  static flags = {
    force: Flags.boolean({
      char: 'f',
      description: 'Overwrite the output file if it already exists',
      required: false,
    }),
    file: Flags.string({
      char: 'f',
      description: 'The file to write the OpenAPI description to',
      required: false,
    }),
    format: Flags.string({
      description: 'The format to generate the OpenAPI description [possible values: yaml]',
      required: false,
    }),
  }

  async run(): Promise<void> {
    const {flags} = await this.parse(Openapi)

    const deprecatedOpenapiFile = path.join(process.cwd(), 'openapi-spec.yml')
    if (fs.existsSync(deprecatedOpenapiFile)) {
      ux.warn('You are having the deprecated file "openapi-spec.yml" in your project. Please consider renaming it to "openapi.yaml".')
    }

    let outputPath = flags.file ?? path.join(process.cwd(), 'openapi.yaml')
    const outputFileExists = fs.existsSync(outputPath)

    let overwriteExistingFile = false
    if (outputFileExists && !flags.force) {
      overwriteExistingFile = await confirm({message: 'Do you want to overwrite the existing file?', default: false})
    }
    if (!outputFileExists || flags.force) {
      overwriteExistingFile = true
    }

    if (!overwriteExistingFile) {
      outputPath = `./openapi-${randomString(5)}.yaml`
    }

    const tasks = new Listr([
      {
        title: 'Ensuring latest base image',
        task: () => executeAsyncCommand(`docker pull ${Openapi.IMAGE}`),
      },
      {
        title: 'Writing generated output',
        task: async () => {
          const {stderr, stdout} = await executeAsyncCommand(`docker run -v "$(pwd):/workspace" ${Openapi.IMAGE} openapi`)
          if (stderr) {
            throw new Error('Failed to generate OpenAPI description: ' + stderr)
          }
          fs.writeFileSync(outputPath, stdout)
        }
      }
    ])

    ux.stdout()
    ux.stdout('Generating OpenAPI description:')
    await tasks.run()
      .then(() => {
        ux.stdout()
        ux.stdout(`File created at: ${path.join(outputPath)}`)
      })
      .catch(error => this.error(error))
  }
}
