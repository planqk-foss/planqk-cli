import {ux} from '@oclif/core'
import fs from 'fs-extra';
import path from 'node:path';

import {zipProjectFolder} from '../../lib/up/zip';
import {AbstractCommand} from '../../model/command'

export default class Compress extends AbstractCommand {
  static description = 'Compresses the current project and creates a ZIP file'
  static examples = [
    '$ planqk compress',
  ]

  async run(): Promise<void> {
    // warn the user if no openapi file is found
    const openapiFile = path.join(process.cwd(), 'openapi.yml')
    if (!fs.existsSync(openapiFile)) {
      ux.warn('No OpenAPI file (openapi.yml) found in the current directory. You may want to create one using the "planqk openapi" command.')
      ux.stdout()
    }

    ux.action.start('Compressing project')

    const targetDirectory = process.cwd()
    const targetFilename = 'planqk.zip'

    const targetFilePath = path.join(targetDirectory, '/', targetFilename)

    // remove the file if it already exists
    if (fs.existsSync(targetFilePath)) {
      fs.unlinkSync(targetFilePath)
    }

    await zipProjectFolder('./', targetDirectory, targetFilename)

    ux.action.stop('Succeeded.')

    ux.stdout(`\nFile created at: ${targetFilePath}`)
    this.exit()
  }
}
