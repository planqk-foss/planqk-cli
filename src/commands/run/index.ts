import {Args, Flags, ux} from '@oclif/core'
import {waitUntil} from 'async-wait-until'
import {readJsonSync} from 'fs-extra'
import inquirer from 'inquirer'
import fs from 'node:fs'
import path from 'node:path'

import {JobDto, JobDtoStatusEnum} from '../../client/planqk'
import {AuthenticatedCommand} from '../../model/command'
import ManagedServiceConfig from '../../model/managed-service-config'
import {defaultBasePath} from '../../model/user-config';
import PlanqkService from '../../service/planqk-service'
import {readServiceConfig} from '../../service/service-config-service'

export default class Run extends AuthenticatedCommand {
  static args = {
    serviceId: Args.string(),
  }
  static description = 'Creates a job execution of a PLANQK Service'
  static examples = [
    '$ planqk run',
    '$ planqk run --detached',
    '$ planqk run -d \'{"values": [10,12]}\' -p \'{"round_up": true}\'',
    '$ planqk run --data-file=./input/data.json --params-file=./input/params.json',
  ]
  static flags = {
    data: Flags.string({char: 'd', description: 'Input data as JSON string.', required: false}),
    'data-file': Flags.string({
      aliases: ['data-file'],
      description: 'Relative path to file containing input data.',
      required: false,
    }),
    detached: Flags.boolean({
      description: 'Executes the job in detached mode, i.e., without waiting for it to finish.',
      required: false,
    }),
    params: Flags.string({char: 'p', description: 'Parameters as JSON string.', required: false}),
    'params-file': Flags.string({
      aliases: ['params-file'],
      description: 'Relative path to file containing params.',
      required: false,
    }),
  }
  planqkService!: PlanqkService

  async getInputData(filePath: string): Promise<string | undefined> {
    let file = filePath

    // if the file path is not absolute, try to find it in the current working directory
    if (!fs.existsSync(filePath)) {
      file = path.join(process.cwd(), filePath)
    }

    if (!fs.existsSync(file)) {
      const response = await inquirer.prompt({
        message: `No input file found under ${filePath}. Run without input data?`,
        name: 'confirm',
        type: 'confirm',
      })
      if (response.confirm) return
      this.exit()
    }

    return JSON.stringify(readJsonSync(file, {encoding: 'utf8'}))
  }

  async getParams(filePath: string): Promise<string | undefined> {
    let file = filePath

    // if the file path is not absolute, try to find it in the current working directory
    if (!fs.existsSync(filePath)) {
      file = path.join(process.cwd(), filePath)
    }

    if (!fs.existsSync(file)) {
      const response = await inquirer.prompt({
        message: `No params file found under ${filePath}. Run without params?`,
        name: 'confirm',
        type: 'confirm',
      })
      if (response.confirm) return
      this.exit()
    }

    return JSON.stringify(readJsonSync(file, {encoding: 'utf8'}))
  }

  async init(): Promise<void> {
    await super.init()
    this.planqkService = new PlanqkService(this, this.config, this.userConfig)
  }

  async run(): Promise<void> {
    const {args} = await this.parse(Run)
    const {flags} = await this.parse(Run)

    let {serviceId} = args

    // if no service id is provided, try to read it from the planqk.json
    if (!serviceId) {
      try {
        const serviceConfig: ManagedServiceConfig = readServiceConfig(process.cwd())
        serviceId = serviceConfig.serviceId
      } catch {
        ux.error('Missing service id. Please provide a service id as argument or set it in the planqk.json of your project.')
      }
    }

    if (!serviceId) ux.error('Missing service id. Please provide a service id as argument or set it in the planqk.json of your project.')

    const service = await this.planqkService.getService(serviceId)
    const serviceDefinitionId = service.serviceDefinitions![0].id!

    const data = flags.data ?? (await this.getInputData(flags['data-file'] || './input/data.json'))
    const params = flags.params ?? (await this.getParams(flags['params-file'] || './input/params.json'))

    const payload = {
      inputData: data,
      parameters: params,
      persistResult: false,
      serviceDefinitionId,
    }

    let job: JobDto
    job = await this.planqkService.runJob(payload)

    if (flags.detached) {
      ux.stdout(`Job (${job.id}) created.`)
      this.exit()
    }

    ux.action.start(`Running Job (${job.id})`)

    try {
      await waitUntil(async () => {
            job = await this.planqkService.getJobById(job.id as string)
            return job.status === JobDtoStatusEnum.Succeeded || job.status === JobDtoStatusEnum.Failed || job.status === JobDtoStatusEnum.Cancelled
          },
          {
            intervalBetweenAttempts: 5000, // every 5 seconds
            timeout: 10 * 60 * 1000, // 10 minute timeout
          })
    } catch {
      // ignore wait until next attempt
    }

    const basePath = this.userConfig.endpoint?.basePath || defaultBasePath
    const jobDetailsLink = `${basePath}/jobs/${job.id}`
    switch (job.status) {
      case JobDtoStatusEnum.Cancelled: {
        ux.action.stop('Job canceled.')
        ux.stdout(`See details at \u001B]8;;${jobDetailsLink}\u0007${jobDetailsLink}\u001B]8;;\u0007\``)

        break;
      }

      case JobDtoStatusEnum.Failed: {
        ux.action.stop('Job failed.')
        ux.stdout(`See details at \u001B]8;;${jobDetailsLink}\u0007${jobDetailsLink}\u001B]8;;\u0007\``)

        break;
      }

      case JobDtoStatusEnum.Succeeded: {
        ux.action.stop('Job succeeded.')
        ux.stdout(`See result at \u001B]8;;${jobDetailsLink}\u0007${jobDetailsLink}\u001B]8;;\u0007\``)

        break;
      }

      default: {
        ux.action.stop('Timeout polling status.')
        ux.stdout(`See status details at \u001B]8;;${jobDetailsLink}\u0007${jobDetailsLink}\u001B]8;;\u0007\``)
      }
    }

    this.exit()
  }
}
