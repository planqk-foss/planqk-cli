import {confirm} from '@inquirer/prompts';
import {Flags, ux} from '@oclif/core'
import {waitUntil} from 'async-wait-until'
import fs from 'node:fs'
import path from 'node:path'

import {
  BuildJobDto,
  BuildJobDtoStatusEnum,
  BuildJobDtoStepEnum,
  ServiceDefinitionDto,
  ServiceDto,
  ValidationResultStateEnum,
} from '../../client/planqk'
import {streamToBlob} from '../../lib/stream-to-blob'
import {getDescriptionText} from '../../lib/up/get-description';
import {zipProjectFolder} from '../../lib/up/zip';
import {AuthenticatedCommand} from '../../model/command'
import ManagedServiceConfig from '../../model/managed-service-config'
import PlanqkService from '../../service/planqk-service'
import {readServiceConfig, writeServiceConfig} from '../../service/service-config-service'

interface ServiceFiles {
  openapi: Blob | undefined,
  descriptionText: string
  sourceCode: Blob,
}

export default class Up extends AuthenticatedCommand {
  static description = 'Creates or updates a PLANQK service'

  static examples = [
    '$ planqk up',
  ]

  static flags = {
    silent: Flags.boolean({
      description: 'Suppresses all outputs, helpful when executed in a CI/CD pipeline.',
      required: false,
    }),
  }

  planqkService!: PlanqkService

  async getOpenapi(): Promise<Blob | undefined> {
    const openapiPath = path.join(process.cwd(), 'openapi.yml')

    if (fs.existsSync(openapiPath)) {
      const apiDefinition = fs.createReadStream(openapiPath, 'utf8')
      return streamToBlob(apiDefinition)
    }

    return undefined
  }

  async init(): Promise<void> {
    await super.init()
    this.planqkService = new PlanqkService(this, this.config, this.userConfig)
  }

  async run(): Promise<void> {
    const {flags} = await this.parse(Up)

    const serviceConfig: ManagedServiceConfig = readServiceConfig(process.cwd())

    let createNewService = true
    const silentMode = flags.silent

    if (!('descriptionFile' in serviceConfig)) {
      ux.stdout('Warning: Since version 2.14.0, the "description" property in planqk.json has changed to "descriptionFile".\nFor more information, visit: https://docs.planqk.de/planqk-json-reference.html\n')
    }

    const deprecatedOpenapiFile = path.join(process.cwd(), 'openapi-spec.yml')
    if (fs.existsSync(deprecatedOpenapiFile)) {
      ux.warn('You are having the deprecated file "openapi-spec.yml" in your project. Please consider renaming it to "openapi.yaml".')
    }

    // if service already exists we update it
    const serviceExistsInPlatform = await this.checkIfServiceExistsInPlatform(serviceConfig);

    if (serviceConfig.serviceId && !serviceExistsInPlatform) {
      ux.stdout(`We could not find a service with id '${serviceConfig.serviceId}' in the current context!`)

      createNewService = await confirm({message: 'Do you want to create a new service?'});
      if (!createNewService) {
        this.exit();
      }
    }

    const serviceFiles: ServiceFiles = {
      openapi: await this.getOpenapi(),
      descriptionText: await getDescriptionText(process.cwd()),
      sourceCode: await zipProjectFolder('./'),
    }

    const service = serviceExistsInPlatform ?
        await this.updateService(serviceConfig, serviceFiles, silentMode) :
        await this.createService(serviceConfig, serviceFiles, silentMode)

    const serviceDefinition = service && service.serviceDefinitions && service.serviceDefinitions[0]

    if (!serviceDefinition) {
      this.error('Service definition not found')
    }

    const buildJob = await this.buildServiceJob(service, serviceDefinition, silentMode);

    if (buildJob && buildJob.status === BuildJobDtoStatusEnum.Success) {
      const msg = `Service ${serviceExistsInPlatform ? 'updated' : 'created'} \u{1F680}`

      if (silentMode) {
        ux.stdout(msg)
      } else {
        ux.action.stop(msg)
      }
    } else if (buildJob && buildJob.status === BuildJobDtoStatusEnum.Failure) {
      const {validationResult} = buildJob
      let reason = 'Failed creating service'

      if (validationResult?.state === ValidationResultStateEnum.Error && validationResult.summary) {
        reason = `${reason}: ${validationResult.summary}`
      }

      if (silentMode) {
        ux.stdout(reason)
      } else {
        ux.action.stop(reason)
      }
    } else {
      const msg = 'Still pending, please check the PLANQK UI to determine the status'
      if (silentMode) {
        ux.stdout(msg)
      } else {
        ux.action.stop(msg)
      }
    }

    this.exit()
  }

  private async buildServiceJob(service: ServiceDto, serviceDefinition: ServiceDefinitionDto, silentMode: boolean) {
    let buildJob: BuildJobDto | undefined

    try {
      await waitUntil(async () => {
            buildJob = await this.planqkService!.getBuildJob(service!, serviceDefinition!)

            if (!silentMode) {
              if (buildJob.step === BuildJobDtoStepEnum.BuildImage) {
                ux.action.start('Building Image (1/2)')
              } else if (buildJob.step === BuildJobDtoStepEnum.PushImage) {
                ux.action.start('Pushing Image (2/2)')
              }
            }

            return buildJob.status === BuildJobDtoStatusEnum.Success || buildJob.status === BuildJobDtoStatusEnum.Failure
          },
          {
            intervalBetweenAttempts: 5000, // every 5 seconds
            timeout: silentMode ? 60 * 60 * 1000 : 20 * 60 * 1000, // silent mode: 60 minutes, normal: 20 minutes
          })
    } catch (error) {
      this.error('Error while waiting for the build job to finish: ' + error)
    }

    return buildJob;
  }

  private async checkIfServiceExistsInPlatform(serviceConfig: ManagedServiceConfig): Promise<boolean> {
    if (serviceConfig.serviceId) {
      try {
        await this.planqkService.getService(serviceConfig.serviceId)
        return true
      } catch {
        return false
      }
    }

    return false
  }

  private async createService(serviceConfig: ManagedServiceConfig,
                              files: ServiceFiles,
                              silentMode: boolean): Promise<ServiceDto> {
    if (!silentMode) {
      ux.action.start('Creating service')
    }

    const service = await this.planqkService.createService(serviceConfig, files.sourceCode, files.openapi)
    serviceConfig.serviceId = service?.id
    writeServiceConfig(process.cwd(), serviceConfig)

    // update description separately when creating a new service
    if (files.descriptionText) {
      await this.planqkService.updateDescription(serviceConfig, files.descriptionText)
    }

    // make sure we update the OpenAPI description if it exists
    if (files.openapi) {
      await this.planqkService.updateOpenapi(serviceConfig, files.openapi)
    }

    return service
  }

  private async updateService(serviceConfig: ManagedServiceConfig, files: ServiceFiles, silentMode: boolean): Promise<ServiceDto> {
    if (!silentMode) {
      ux.action.start('Updating service')
    }

    return this.planqkService.updateService(serviceConfig, files.descriptionText, files.sourceCode, files.openapi)
  }
}
