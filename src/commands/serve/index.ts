import {Flags, ux} from '@oclif/core'
import Listr from 'listr'
import terminalLink from 'terminal-link';
import {executeAsyncCommand, executeCommand} from '../../lib/command';
import {randomString} from '../../lib/random-string'
import {AbstractCommand} from '../../model/command'

export default class Serve extends AbstractCommand {
  static CONTAINER_NAME = 'planqk-cli-serve'
  static IMAGE = 'registry.gitlab.com/planqk-foss/planqk-cli-serve:latest'

  static description = 'Runs your project in a containerized environment and expose it through a ' +
    'local web server, similarly to how PLANQK would run your code. The local web server exposes the ' +
    'same HTTP endpoints to start a service execution, to check the status of running executions, to cancel ' +
    'executions, and to retrieve execution results.'

  static examples = [
    '$ planqk serve',
    '$ planqk serve -p <port>',
  ]

  static flags = {
    port: Flags.integer({
      char: 'p',
      description: 'The port on which the local web server accepts requests',
      required: false,
    }),
  }

  async run(): Promise<void> {
    const {flags} = await this.parse(Serve)
    const hostPort = flags.port ?? 8081

    const containerName = `${Serve.CONTAINER_NAME}-${randomString(10)}`

    const tasks = new Listr([
      {
        task: () => executeAsyncCommand(`docker pull ${Serve.IMAGE}`),
        title: 'Ensuring latest base image',
      },
      {
        task: () => executeAsyncCommand(`docker run -e PORT=${hostPort} -p ${hostPort}:${hostPort} -v "$(pwd):/workspace" --name ${containerName} ${Serve.IMAGE}`),
        title: 'Building container',
      },
      {
        task: () => Promise.resolve(),
        title: 'Starting container',
      },
    ])

    ux.stdout('Starting service locally...')
    tasks.run()
      .then(() => {
        const baseUrl = `http://localhost:${hostPort}`
        const docsUrl = `${baseUrl}/docs`

        ux.stdout('\nLocal service running at:')
        ux.stdout(terminalLink(baseUrl, baseUrl))
        ux.stdout('\nDocumentation and interactive API client available at:')
        ux.stdout(terminalLink(docsUrl, docsUrl))
        ux.stdout('')

        try {
          executeCommand(`docker start -i -a ${containerName}`)
        } catch {
          this.error(`There might be problems with your dependencies. Don't forget to verify the requirements.txt file`)
        }
      })
      .catch(error => this.error(error))
      .finally(() => {
        ux.stdout('\nStopping and removing container...')
        executeCommand(`docker stop ${containerName} > /dev/null 2>&1`)
        return executeAsyncCommand(`docker rm ${containerName} > /dev/null 2>&1`)
      })
  }
}
