import {Args, ux} from '@oclif/core'
import inquirer from 'inquirer'

import Account from '../../model/account'
import {AuthenticatedCommand} from '../../model/command'
import PlanqkService from '../../service/planqk-service'
import {writeUserConfig} from '../../service/user-config-service'

interface Context {
  displayName: string
  id: string
  isOrganization: boolean
}

export default class SetContext extends AuthenticatedCommand {
  static args = {
    contextId: Args.string(),
  }

  static description = 'Set the current context, i.e., the personal or organization account you are currently working with.'

  static examples = [
    '$ planqk set-context',
    '$ planqk set-context <context-id>',
  ]

  planqkService!: PlanqkService

  getPromptOptions(accounts: Account[]): { name: string; value: Account }[] {
    const options = []
    for (const account of accounts) {
      options.push({
        name: account.isOrganization ? `${account.displayName} (Organization)` : account.displayName,
        value: account,
      })
    }

    return options
  }

  async init(): Promise<void> {
    await super.init()
    this.planqkService = new PlanqkService(this, this.config, this.userConfig)
  }

  async run(): Promise<void> {
    const {args} = await this.parse(SetContext)

    const {contextId} = args
    let selectedContext: Context | undefined

    if (!contextId) {
      ux.action.start('Loading available contexts')
    }

    const accounts: Account[] = await this.planqkService.getAccounts()

    if (!contextId) {
      ux.action.stop('Done')
    }

    if (contextId) {
      selectedContext = accounts.find(account => account.id === contextId)
    } else {
      const response = await inquirer.prompt({
        choices: this.getPromptOptions(accounts),
        message: 'Choose a context',
        name: 'context',
        type: 'list',
      })
      selectedContext = response.context
    }

    if (!selectedContext) {
      this.error(`Context with id "${contextId}" not found.`)
    }

    const userConfig = {
      ...this.userConfig,
      context: selectedContext!,
    }
    writeUserConfig(this.config.configDir, userConfig)

    ux.stdout(`Switched to context "${selectedContext?.displayName}".`)
  }
}
