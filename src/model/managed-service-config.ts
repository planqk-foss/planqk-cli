export default interface ManagedServiceConfig {
  descriptionFile?: string;
  name: string;
  resources?: ResourceConfiguration;
  runtime?: Runtime;
  serviceId?: string;
}

export enum Runtime {
  DOCKER = 'DOCKER',
  PYTHON = 'PYTHON',
  PYTHON_TEMPLATE = 'PYTHON_TEMPLATE'
}

export interface ResourceConfiguration {
  cpu: number;
  gpu?: GpuConfiguration
  memory: number;
}

export interface GpuConfiguration {
  count: number;
  type: GpuType;
}

export enum GpuType {
  NVIDIA_TESLA_A100 = 'NVIDIA_TESLA_A100',
  NVIDIA_TESLA_P100 = 'NVIDIA_TESLA_P100',
  NVIDIA_TESLA_T4 = 'NVIDIA_TESLA_T4',
  NVIDIA_TESLA_V100 = 'NVIDIA_TESLA_V100',
}
