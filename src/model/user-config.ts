export enum AuthType {
  API_KEY = 'API_KEY'
}

export interface Authentication {
  type: AuthType;
  value: string;
}

export interface Endpoint {
  basePath: string;
  defaultHeaders: Record<string, string>;
}

export interface Context {
  displayName: string;
  id: string;
  isOrganization: boolean;
}

export const defaultBasePath = 'https://platform.planqk.de'

export default interface UserConfig {
  auth?: Authentication;
  context?: Context;
  endpoint?: Endpoint;
}
