export default interface Account {
  displayName: string;
  id: string;
  isOrganization: boolean;
}
