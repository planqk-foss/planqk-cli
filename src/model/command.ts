import {Command, ux} from '@oclif/core'
import {Config} from '@oclif/core/lib/config'

import AuthService from '../service/auth-service'
import {readUserConfig, writeUserConfig} from '../service/user-config-service'
import UserConfig from './user-config'

export abstract class AbstractCommand extends Command {
  userConfig: UserConfig

  constructor(argv: string[], config: Config) {
    super(argv, config)
    this.userConfig = readUserConfig(this.config.configDir)
  }
}

export abstract class AuthenticatedCommand extends AbstractCommand {
  async getContextOutputString(userConfig: UserConfig): Promise<string> {
    if (userConfig && userConfig.context) {
      const type = userConfig.context.isOrganization ? 'Organization' : 'Personal'
      return `${userConfig.context.displayName} (${type})`
    }

    return 'No context set. Use "planqk set-context" to set a context.'
  }

  async init(): Promise<void> {
    await super.init()

    if (!this.userConfig.auth) {
      throw new Error('No credentials provided. Use the "login" command to log-in.')
    }

    const auth = new AuthService(this.userConfig)
    try {
      await auth.authenticate(this.userConfig.auth.value)
    } catch {
      writeUserConfig(this.config.configDir, {
        ...this.userConfig,
        auth: undefined,
        context: undefined,
      })
      throw new Error('Invalid credentials provided. Use the "login" command to log-in.')
    }

    ux.stdout('Current context: ' + await this.getContextOutputString(this.userConfig))
  }
}
