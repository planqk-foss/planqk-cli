import {UserDto} from '../client/users';
import {fetchOrThrow, PlanqkError} from '../lib/fetch'
import UserConfig, {defaultBasePath} from '../model/user-config'
import UserService from './user-service';

export default class AuthService {
  userConfig: UserConfig
  userService: UserService

  constructor(userConfig: UserConfig) {
    this.userConfig = userConfig
    this.userService = new UserService(userConfig)
  }

  async authenticate(apiKey: string): Promise<UserDto> {
    try {
      const basePath = (this.userConfig.endpoint?.basePath || defaultBasePath) + '/qc-catalog'
      await fetchOrThrow(basePath + '/authorize', {
        headers: {'X-Auth-Token': apiKey},
        method: 'POST',
      })
      return await this.userService.getCurrentUser(apiKey)
    } catch (error) {
      if (error instanceof PlanqkError) {
        if (error.response.status === 401) {
          throw new Error('Invalid API key')
        }

        const errorMessage = await error.getErrorMessage()
        throw new Error(errorMessage)
      }

      console.error(JSON.stringify(error))

      throw new Error('Internal error occurred, please contact your PLANQK administrator')
    }
  }
}
