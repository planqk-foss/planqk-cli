import {Command} from '@oclif/core'
import {Config} from '@oclif/core/lib/config'

import {
  BuildJobDto,
  Configuration,
  CpuConfiguration,
  CreateJobRequest,
  CreateManagedServiceRuntimeEnum,
  GpuConfiguration,
  JobDto,
  MemoryConfiguration,
  ServiceDefinitionDto,
  ServiceDto,
  ServiceOverviewDto,
  ServicePlatformJobsApi,
  ServicePlatformServicesApi,
} from '../client/planqk'
import {handleError} from '../lib/error';
import {fetchOrThrow} from '../lib/fetch'
import Account from '../model/account'
import ManagedServiceConfig from '../model/managed-service-config'
import UserConfig, {defaultBasePath} from '../model/user-config'
import {CommandService} from './command-service'

export default class PlanqkService extends CommandService {
  jobApi: ServicePlatformJobsApi
  serviceApi: ServicePlatformServicesApi

  constructor(cmd: Command, config: Config, userConfig: UserConfig) {
    super(cmd, config, userConfig)

    const basePath = (userConfig.endpoint?.basePath || defaultBasePath) + '/qc-catalog'
    const defaultHeaders = userConfig.endpoint?.defaultHeaders || {}
    const apiKey = userConfig.auth?.value as string

    this.serviceApi = new ServicePlatformServicesApi(new Configuration({
      apiKey,
      basePath,
      headers: defaultHeaders,
    }))
    this.jobApi = new ServicePlatformJobsApi(new Configuration({
      apiKey,
      basePath,
      headers: defaultHeaders,
    }))
  }

  async createService(serviceConfig: ManagedServiceConfig, sourceCode: Blob, apiDefinition?: Blob): Promise<ServiceDto> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined

    const milliCpus = (serviceConfig.resources?.cpu || 1) * 1000
    const memoryInMegabytes = (serviceConfig.resources?.memory || 2) * 1024

    const gpuAccelerator = serviceConfig.resources?.gpu?.type || 'NONE'
    const gpuCount = serviceConfig.resources?.gpu?.count || 0

    try {
      return await this.serviceApi.createManagedService({
          apiDefinition,
          gpuAccelerator,
          gpuCount,
          memoryInMegabytes,
          milliCpus,
          name: serviceConfig.name,
          runtime: serviceConfig.runtime as CreateManagedServiceRuntimeEnum,
          userCode: sourceCode,
          xOrganizationId: organizationId,
        },
      )
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async getAccounts(): Promise<Account[]> {
    const basePath = (this.userConfig.endpoint?.basePath || defaultBasePath) + '/qc-catalog'
    try {
      const response = await fetchOrThrow(basePath + '/my/accounts', {
        headers: {'X-Auth-Token': this.userConfig.auth!.value},
      })
      return (await response.json()) as Account[]
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async getBuildJob(service: ServiceDto, serviceDefinition: ServiceDefinitionDto): Promise<BuildJobDto> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    try {
      return await this.serviceApi.getBuildStatus({
        serviceId: service.id!,
        versionId: serviceDefinition.id!,
        xOrganizationId: organizationId,
      })
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async getJobById(id: string): Promise<JobDto> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    try {
      return await this.jobApi.getJob({id, xOrganizationId: organizationId})
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async getService(id: string): Promise<ServiceDto> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    try {
      return await this.serviceApi.getService({id, xOrganizationId: organizationId})
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async getServices(): Promise<ServiceOverviewDto[]> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    try {
      return await this.serviceApi.getServices({xOrganizationId: organizationId})
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async runJob(payload: CreateJobRequest): Promise<JobDto> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    try {
      return await this.jobApi.createJob({createJobRequest: payload, xOrganizationId: organizationId})
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async updateDescription(serviceConfig: ManagedServiceConfig, description: string): Promise<void> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    const service = await this.serviceApi.getService({id: serviceConfig.serviceId!, xOrganizationId: organizationId})
    const serviceDefinition = service && service.serviceDefinitions && service.serviceDefinitions[0]

    try {
      await this.serviceApi.updateServiceVersion({
        serviceId: service.id!,
        updateVersionRequest: {description},
        versionId: serviceDefinition!.id!,
        xOrganizationId: organizationId,
      })
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }

  async updateOpenapi(serviceConfig: ManagedServiceConfig, apiDefinition: Blob): Promise<void> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    const service = await this.serviceApi.getService({id: serviceConfig.serviceId!, xOrganizationId: organizationId})
    const serviceDefinition = service && service.serviceDefinitions && service.serviceDefinitions[0]

    if (apiDefinition) {
      await this.serviceApi.updateApiDefinition({
        file: apiDefinition,
        serviceId: service.id!,
        versionId: serviceDefinition!.id!,
        xOrganizationId: organizationId,
      })
    }
  }

  async updateService(serviceConfig: ManagedServiceConfig, description: string, sourceCode: Blob, apiDefinition?: Blob): Promise<ServiceDto> {
    const organizationId = this.userConfig.context?.isOrganization ? this.userConfig.context.id : undefined
    const service = await this.serviceApi.getService({id: serviceConfig.serviceId!, xOrganizationId: organizationId})
    const serviceDefinition = service && service.serviceDefinitions && service.serviceDefinitions[0]

    const milliCpus = (serviceConfig.resources?.cpu || 1) * 1000
    const cpuConfiguration: CpuConfiguration = {
      amount: milliCpus,
      type: 'cpu',
      unit: 'm',
    }

    const memoryInMegabytes = (serviceConfig.resources?.memory || 2) * 1024
    const memoryConfiguration: MemoryConfiguration = {
      amount: memoryInMegabytes,
      type: 'memory',
      unit: 'M',
    }

    const gpuAccelerator = serviceConfig.resources?.gpu?.type || 'NONE'
    const gpuCount = serviceConfig.resources?.gpu?.count || 0
    const gpuConfiguration: GpuConfiguration = {
      accelerator: gpuAccelerator,
      amount: gpuCount,
      type: 'gpu',
    }

    try {
      if (description) {
        await this.serviceApi.updateServiceVersion({
          serviceId: service.id!,
          updateVersionRequest: {
            description,
          },
          versionId: serviceDefinition!.id!,
          xOrganizationId: organizationId,
        })
      }

      await this.serviceApi.updateResourceConfiguration({
        serviceId: service.id!,
        updateResourceConfigurationRequest: cpuConfiguration,
        versionId: serviceDefinition!.id!,
        xOrganizationId: organizationId,
      })

      await this.serviceApi.updateResourceConfiguration({
        serviceId: service.id!,
        updateResourceConfigurationRequest: memoryConfiguration,
        versionId: serviceDefinition!.id!,
        xOrganizationId: organizationId,
      })

      await this.serviceApi.updateResourceConfiguration({
        serviceId: service.id!,
        updateResourceConfigurationRequest: gpuConfiguration,
        versionId: serviceDefinition!.id!,
        xOrganizationId: organizationId,
      })

      if (apiDefinition) {
        await this.serviceApi.updateApiDefinition({
          file: apiDefinition,
          serviceId: service.id!,
          versionId: serviceDefinition!.id!,
          xOrganizationId: organizationId,
        })
      }

      await this.serviceApi.updateSourceCodePost({
        file: sourceCode,
        serviceId: service.id!,
        versionId: serviceDefinition!.id!,
        xOrganizationId: organizationId,
      })
      return service
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }
}
