import {Configuration, UserDto, UserSettingsApi} from '../client/users'
import {handleError} from '../lib/error';
import UserConfig, {defaultBasePath} from '../model/user-config'

export default class UserService {
  basePath: string;
  defaultHeaders: Record<string, string>;
  userSettingsApi: UserSettingsApi

  constructor(userConfig: UserConfig) {
    this.basePath = (userConfig.endpoint?.basePath || defaultBasePath) + '/user-service'
    this.defaultHeaders = userConfig.endpoint?.defaultHeaders || {}
    const apiKey = userConfig.auth?.value as string

    this.userSettingsApi = new UserSettingsApi(new Configuration({
      apiKey,
      basePath: this.basePath,
      headers: this.defaultHeaders,
    }))
  }

  async getCurrentUser(apiKey: string): Promise<UserDto> {
    const userSettingsApi = new UserSettingsApi(new Configuration({
      apiKey,
      basePath: this.basePath,
      headers: this.defaultHeaders,
    }))

    try {
      return await userSettingsApi.getUser()
    } catch (error) {
      const errorMessage = await handleError(error)
      throw new Error(errorMessage)
    }
  }
}
