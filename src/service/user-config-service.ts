import {createFileSync, readJsonSync, writeJsonSync} from 'fs-extra'
import fs from 'node:fs'
import path from 'node:path'

import UserConfig from '../model/user-config'

export const readUserConfig = (location: string): UserConfig => {
  const file = path.join(location, 'config.json')

  if (!fs.existsSync(file)) {
    return {}
  }

  return readJsonSync(file, {encoding: 'utf8'})
}

export const writeUserConfig = (location: string, userConfig: UserConfig): UserConfig => {
  const file = path.join(location, 'config.json')

  if (!fs.existsSync(file)) {
    createFileSync(file)
  }

  writeJsonSync(file, userConfig, {encoding: 'utf8', spaces: 2})

  return readUserConfig(location)
}
