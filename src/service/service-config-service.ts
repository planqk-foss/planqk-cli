import {createFileSync, readJsonSync, writeJsonSync} from 'fs-extra'
import fs from 'node:fs'
import path from 'node:path'

import ManagedServiceConfig from '../model/managed-service-config'

export const readServiceConfig = (location: string): ManagedServiceConfig => {
  const file = path.join(location, 'planqk.json')

  if (!fs.existsSync(file)) {
    throw new Error('No planqk.json file found in current directory')
  }

  return readJsonSync(file, {encoding: 'utf8'})
}

export const writeServiceConfig = (location: string, serviceConfig: ManagedServiceConfig): ManagedServiceConfig => {
  const file = path.join(location, 'planqk.json')

  if (!fs.existsSync(file)) {
    createFileSync(file)
  }

  writeJsonSync(file, serviceConfig, {encoding: 'utf8', spaces: 2})

  return readServiceConfig(location)
}
